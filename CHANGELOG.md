# 1.0.6

- Added usage example to README.md and links to dart references on using exit / return codes

# 1.0.5

- Switch to pana for ci

# 1.0.4

- Removed dart 1.x from ci

# 1.0.3

- Updated to min dart sdk 2.12 for null saftey
- Updated comments to dartdoc style

# 1.0.2

- Updated README.md formatting

# 1.0.1

- Added ci support for dart SDK 1.x

# 1.0.0

- Port from <sysexits.h>
