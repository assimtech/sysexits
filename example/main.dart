#!/usr/bin/env dart

import 'dart:io';
import 'package:sysexits/sysexits.dart';

void printUsage() {
  print('Usage:');
  print('-h, --[no-]help    Show help');
}

void main(List<String> argv) {
  exitCode = EX_OK; // presume success

  for (String arg in argv) {
    switch(arg) {
      case '-h':
      case '--help':
        printUsage();
        exit(EX_OK);
      default:
        printUsage();
        exit(EX_USAGE);
    }
  }

  print('Do some things');
}
